package Range

import (
	"errors"
	"fmt"
	"math"
	"os"
)

/*
	Range(stop) -> _Range object
	Range(start, stop[, step]) -> _Range object

	Return an object that produces a sequence of integers from start (inclusive)
	to stop (inclusive) by step. Range(i, j) produces i, i+1, i+2, ..., j.

	When stop is omitted, start defaults to 1.
	Range(4) produces 1, 2, 3, 4.

	len(Range(n)) == n
	When step is given, it specifies the increment (or decrement).
*/
type _Range struct {
	start int
	stop int
	step int
	len int
}

// constructor
func Range(args ...int) *_Range {
	var start int = 1
	var stop int
	var step int = 1

	switch len(args) {
		case 0:
			error := errors.New("Range expected at least 1 argument, got 0")
			fmt.Fprintf(os.Stderr, "error: %v\n", error)
			os.Exit(1)
		case 1:
			stop = args[0]
		default:
			start = args[0]
			stop = args[1]

			if len(args) > 2 {
				step = args[2]
			}
	}

	r := &_Range{start: start, stop: stop, step: step}

	sign := int(math.Copysign(1, float64(step)))
	length := float64(1 + (stop - start - sign) / step)
	r.len = int(math.Max(length, 0))

	return r
}

func (r _Range) Start() int {
	return r.start
}

func (r _Range) Stop() int {
	return r.stop
}

func (r _Range) Step() int {
	return r.step
}

func (r _Range) String() string {
    // return fmt.Sprintf("Range(%d, %d, %d)", r.start, r.stop, r.step)
    return fmt.Sprintf("%v", r.ToList())
}

func (r _Range) Len() int {
    return r.len
}

func (r _Range) ToList() []int {
	value := r.start
	var arr []int

	if r.step > 0 {
		for value <= r.stop {
        	arr = append(arr, value)
            value += r.step
        }
	} else {
		for value >= r.stop {
			arr = append(arr, value)
			value += r.step
		}
	}

    return arr
}